package Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Model.Produit;

/**
 * Servlet implementation class traitementDetail
 */
@WebServlet("/traitementDetail")
public class traitementDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public traitementDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		String code = request.getParameter("id");
		DAO dao = new DAO();
		Produit p = new Produit();
		
		// Recueil des informations du produit via le code
		p = dao.getProduitByCode(code);
		
		/* Si le produi exixte on l'enregistre en session et on effectue un redirection
		 * Sinon on le redirige sur la page d'acceuil
		 * */
		
		if(p != null) {
			session.setAttribute("produit", p);
			response.sendRedirect("details_recherche.jsp");
		}	
		else {
			response.sendRedirect("index.jsp");
		}
	}
	
}
