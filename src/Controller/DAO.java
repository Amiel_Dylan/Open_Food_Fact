package Controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Model.Produit;

public class DAO {
	
	private boolean connect;
	final static String URL = "jdbc:mysql://localhost/open_food_fact?serverTimezone=Europe/Paris";
	final static String LOGIN = "root";
	final static String PASSWORD = "";
	Connection conn = null;
	
	public DAO() {
		//Chargement du pilote de bases de donn�es
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			System.out.println("MSQL Connector found");
			
		}catch (ClassNotFoundException e) {						
			System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
			}
		}
	
	/**
	 * Methode qui retourne une liste de produit via le nom du produit
	 * @param String Le nom du produit
	 * @return ArrayList<Produit> Une Arraylist des produits retrouv�s
	 * 
	 * */
	public ArrayList<Produit> getProduitByName(String recherche) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Produit> retour = new ArrayList<Produit>();

		// Connexion � la BDD
		try {
			
			con = DriverManager.getConnection(URL, LOGIN, PASSWORD);
			
			// Retourne un produit si il contient une partie du nom en parametre
			ps = con.prepareStatement("SELECT * FROM food WHERE product_name LIKE ?");
			ps.setString(1, "%"+recherche+"%");
			

			// on execute la requete
			rs = ps.executeQuery();
			
			// on parcourt les lignes du r�sultat
			while (rs.next())
				retour.add(new Produit(rs.getString("product_name"),rs.getString("quantity"),
									   rs.getString("code"),rs.getString("brands"),rs.getString("image_small_url"),
									   rs.getString("creator"),rs.getString("ingredients_text"),rs.getString("image_ingredients_small_url"),rs.getString("image_nutrition_small_url")));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;
		
	}
	
	/**
	 * Methode qui retourne une liste de produit suivant l'ingredient recherch�
	 * @param String Le nom du produit
	 * @return ArrayList<Produit> Une Arraylist des produits retrouv�s
	 * 
	 * */
	public ArrayList<Produit> getProduitByIng(String recherche) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Produit> retour = new ArrayList<Produit>();

		// connexion � la base de donn�es
		try {
			
			con = DriverManager.getConnection(URL, LOGIN, PASSWORD);
			ps = con.prepareStatement("SELECT * FROM food WHERE ingredients_text LIKE ?");
			ps.setString(1, "%" + recherche + "%");
			

			// on exécute la requête
			rs = ps.executeQuery();
			// on parcourt les lignes du résultat
			while (rs.next())
				retour.add(new Produit(rs.getString("product_name"),rs.getString("quantity"),
									   rs.getString("code"),rs.getString("brands"),rs.getString("image_small_url"),
									   rs.getString("creator"),rs.getString("ingredients_text"),rs.getString("image_ingredients_small_url"),rs.getString("image_nutrition_small_url")));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;
		
	}
	
	/**
	 * Methode qui retourne une liste de produit via l'id du produit
	 * @param String Le nom du produit
	 * @return ArrayList<Produit> Une Arraylist des produits retrouv�s
	 * 
	 * */
	public Produit getProduitByCode(String code) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Produit retour = null;

		// connexion � la BDD
		try {
			
			con = DriverManager.getConnection(URL, LOGIN, PASSWORD);
			ps = con.prepareStatement("SELECT * FROM food WHERE code = ?");
			ps.setString(1, code);
			

			// on execute la requete
			rs = ps.executeQuery();
			
			// on parcourt les lignes du r�sultat
			while (rs.next())
				retour = new Produit(rs.getString("product_name"),rs.getString("quantity"),
									   rs.getString("code"),rs.getString("brands"),rs.getString("image_small_url"),
									   rs.getString("creator"),rs.getString("ingredients_text"),rs.getString("image_ingredients_small_url"),rs.getString("image_nutrition_small_url"));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;
		
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DAO dao = new DAO();
		ArrayList<Produit> produit = new ArrayList<Produit>();
		
		produit = dao.getProduitByIng("sel");
		System.out.println(produit.toString());
		
	}

}
