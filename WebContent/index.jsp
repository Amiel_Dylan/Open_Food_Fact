<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.*,Model.*"%>
    
<%!
	private Cookie[] cookies;
	private String state = "";
	private String msg = "";
%>

<%
	cookies = request.getCookies(); 
	
	if(cookies != null){
		for (Cookie c : cookies){
				if (c.getName().equals("error")){
			      state = c.getValue();
			  }
		}
	}
	
	if(state.equals("false")){
		msg = "Produit inexistant!";
	}else if(state.equals("empty")){
		msg = "Veuillez renseigner le produit!";
	}
	
	if(session.getAttribute("liste") == null){
		session.setAttribute("liste", new ArrayList<Produit>());
		session.setAttribute("recherche", new ArrayList<Produit>());
	};
	
%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- link Bulma, Bootstrap, Buefy, AOS, FontAwesome-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito|Cookie|Quando|Quattrocento|Quintessential" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="caroussel.css">
    
    <title>Open Food Fact</title>
</head>

<body class="bcg">

    <!-- Corps de la page -->
    <div class="container">

    	<div class="column">
    		<h1 id="titre"> OPEN FOOD FACTS </h1>
    	</div>
    	
    	<div class="column">
    		<h3 id="ss_titre" class="title"> Trouvez toutes les informations dont vous avez besoin sur la plupart des produits du march� ! </h3>
    	</div>
    	
    	<div class="column bloc">
    		<form autocomplete="off" action="traitementRecherche" method="post">
	    		<div class="input-group mb-3 mt-5 autocomplete">
				  	<input type="text" class="form-control form-control-lg" id="imp" name="Nom" placeholder="Nom du produit" aria-label="Nom du produit" aria-describedby="button-addon2">
				 
				  	<div class="input-group-append">
				    	<button class="btn btn-outline-secondary btn-lg" type="submit" id="button-addon2">Rechercher</button>
				  	</div>
				</div>
				<label for="imp" id="label"><%=msg %></label>
			
				<div> <h3 id="under"> <a href="derniere_recherche.jsp"><u> Produits pr�c�demment recherch�s </u></a> </h3></div>
			</form>
    	</div>
    </div>
    
     <script src="script.js"></script>
 

    <!-- Bootstrap, Buefy and AOS JS requirements -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>AOS.init();</script>
</body>


</html>