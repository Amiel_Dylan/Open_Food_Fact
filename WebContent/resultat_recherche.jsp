<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="Controller.*,java.util.*,Model.*"%>

<%
	session = request.getSession();	
	ArrayList<Produit> produit = new ArrayList<Produit>();	
	
	if(session.getAttribute("recherche") != null){
		produit = (ArrayList<Produit>) session.getAttribute("recherche");
	}
%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- link Bulma, Bootstrap, Buefy, AOS, FontAwesome-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito|Cookie|Quando|Quattrocento|Quintessential" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
    
    <title>Open Food Fact</title>
</head>

<body>
    <!-- Corps de la page -->
    <div class="container">
    	<div class="column">
    		<nav class="navbar navbar-light bg-primary">
			  	<a class="navbar-brand" id="nav-brand" href="index.jsp"><h3>Open Food Fact</h3></a>
			  	
			  	<div class=" d-flex flex-row justify-content-left">
			  		<div class="field has-addons">
					  	<div class="control">
					    	<input class="input" type="text" placeholder="recherche par nom">
					  	</div>
					  	<div class="control">
					    	<a class="button is-info">GO</a>
					    </div>
					</div>
					
					<div class="ml-5 field has-addons">
					  	<div class="control">
					    	<input class="input" type="text" placeholder="recherche par ingredients">
					  	</div>
					  	<div class="control">
					    	<a class="button is-info">GO</a>
					    </div>
					</div>
			  	</div>
			  		
			</nav>
    	</div>
    	
    	<div class="column"> 
			<h3 id="res"> <u>Resultat de recherche</u> </h3>	
		</div>
		
		<%
			for(int i = 0; i < produit.size(); i++){
			String nom;
			String masse;
			String marque;
			String code;
			String url;
			
			if(produit.get(i).getNom() ==""){
				nom = "Valeur Inexistante";
			}else{
				nom = produit.get(i).getNom();
			}
		
			if(produit.get(i).getMasse()==""){
				masse = "Valeur Inexistante";
			}else{
				masse = produit.get(i).getMasse();
			}
			
			if(produit.get(i).getMarque()==""){
				marque = "Valeur inexistante";
			}else{
				marque = produit.get(i).getMarque();
			}
			
			code = produit.get(i).getId();
			url = produit.get(i).getImage_url();
			
		%>
			<div class="column">
			 <a href = "traitementDetail?id=<%=code %>"> 
			    <div class="box">
					<div class="columns carte pb-0 mb-8">
						<div clas="column"> 
							<figure id="figure" class="image is-128x128">
							  	<img  id="img" src=<%=url %>>
							</figure>
						</div>
						<div clas="column" id="desc">
							<div class="column" id="name"> <strong><%=nom %></strong> </div>
							<div class="column"> Brand : <%=marque %></div>
							<div class="column" id="masse"> Masse :  <%=masse %></div>
						</div>
						<div clas="column"> 
							<div id="id"> <p><%=code %></p> </div>
						</div>
					</div>
				</div>
			  </a>
			 </div>
		  <%}%>	
    </div>

       

    <!-- Bootstrap, Buefy and AOS JS requirements -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>AOS.init();</script>
</body>


</html>