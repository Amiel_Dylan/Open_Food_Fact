function autocomplete(inp, arr) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      /*close any already open lists of autocompleted values*/
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      /*create a DIV element that will contain the items (values):*/
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      /*append the DIV element as a child of the autocomplete container:*/
      this.parentNode.appendChild(a);
      /*for each item in the array...*/
      for (i = 0; i < arr.length; i++) {
        /*check if the item starts with the same letters as the text field value:*/
        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          /*create a DIV element for each matching element:*/
          b = document.createElement("DIV");
          /*make the matching letters bold:*/
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          /*insert a input field that will hold the current array item's value:*/
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          /*execute a function when someone clicks on the item value (DIV element):*/
          b.addEventListener("click", function(e) {
              /*insert the value for the autocomplete text field:*/
              inp.value = this.getElementsByTagName("input")[0].value;
              /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/
              closeAllLists();
          });
          a.appendChild(b);
        }
      }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        /*If the arrow DOWN key is pressed,
        increase the currentFocus variable:*/
        currentFocus++;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 38) { //up
        /*If the arrow UP key is pressed,
        decrease the currentFocus variable:*/
        currentFocus--;
        /*and and make the current item more visible:*/
        addActive(x);
      } else if (e.keyCode == 13) {
        /*If the ENTER key is pressed, prevent the form from being submitted,*/
        e.preventDefault();
        if (currentFocus > -1) {
          /*and simulate a click on the "active" item:*/
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    /*a function to classify an item as "active":*/
    if (!x) return false;
    /*start by removing the "active" class on all items:*/
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    /*add class "autocomplete-active":*/
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    /*a function to remove the "active" class from all autocomplete items:*/
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
  });
}

/*An array containing all the country names in the world:*/
var produits = ["Belriso"
	,"Escalope de Dinde l'extra Tendre"
	,"Poulet au paprika"
	,"Aiguillettes de Poulet"
	,"Filets de poulet"
	,"Crousty chicken l'original"
	,"Saucisses de poulet saveur chorizo x4"
	,"2 Hachés de canard"
	,"Houmous graines de sesame"
	,"Les Maxis Croq' dés de jambon de dinde et fromage"
	,"Tendres Grignottes saveur barbecue 300g"
	,"Tendres Grignottes natures Le Gaulois 72"
	,"Panés façon Milanaise Finement Citronné x2"
	,"Pané façon Milanaise Aux Fines Herbes"
	,"Emincés de Kangourou marinés aux 3 Poivres"
	,"Crousty Chicken Hot&Spicy"
	,"Crousty Chicken l'original"
	,"Blanc de poulet"
	,"Mortadelle de Volaille"
	,"Blanc de dinde REGHALAL 6 tranches"
	,"Blanc de poulet REGHALAL 6 tranches"
	,"Brochettes de poulet Colombo x3"
	,"Brochettes Olive Romarin x3"
	,"Plateau Gourmand de Poulet"
	,"Tranches fines de canard au poivre"
	,"dinde sauté de dinde"
	,"Poulet à griller aux Herbes de Provence"
	,"Filet de canard au piment d'Espelette"
	,"L'extra tendre filet de poulet jaune"
	,"Chapon farci farce pomme du Val de Loire"
	,"Sel"	
  ,"Sucre"
  ,"Légume"
  ,"Eau"
  ,"Arôme"
  ,"Huiles"
  ,"Huile et graisse végéta"
  ,"Huile végétale"
  ,"Fruit"
  ,"Céréale"
  ,"Blé"
  ,"Glucose"
  ,"Lait"
  ,"Farine de céréales"
  ,"Amidon"
  ,"Farine de blé"
  ,"Épice"
  ,"Graine"
  ,"Arôme naturel"
  ,"Émulsifiant"
  ,"Colorant"
  ,"Levure"
  ,"Œuf"
  ,"Acide citrique"
  ,"Herbe"
  ,"Vitamines"
  ,"Antioxydant"
  ,"Conservateur"
  ,"pepper	"
  ,"Acidifiant"
  ,"Lait en poudre"
  ,"Lécithine"
  ,"Huile de tournesol"
  ,"Oignon"
  ,"Fruits à coque"
  ,"Dextrose"
  ,"Cacao"
  ,"Huile de colza"
  ,"Sirop de glucose"
  ,"Ferment"
  ,"Ail"
  ,"Tomate"
  ,"Viande"
  ,"Épaississant"
  ,"Beurre de cacao"
  ,"Amidon transformé"
  ,"Citrus"
  ,"Stabilisant"
  ,"Vinaigre"
  ,"Crème"
  ,"Lait en poudre écrémé"
  ,"Protéine"
  ,"Pâte de cacao"
  ,"Vitamine C"
  ,"Lécithine de soja"
  ,"Poudre de levure"
  ,"Amidon de mais"
  ,"Huile de palme"
  ,"Jus de fruits"
  ,"Carotte"
  ,"Beurre"
  ,"Viande de porc"
  ,"Soja"
  ,"Poisson"
  ,"Lactose"
  ,"Alcool"
  ,"Fromage"
  ,"Correcteur d'acidité"
  ,"Pectine"
  ,"Protéine animale"
  ,"Protéine de lait"
  ,"Sucre de canne"
  ,"Farine"
  ,"Citron"
  ,"Gélifiant"
  ,"Fructose"
  ,"Sel marin"
  ,"Ferment lactique"
  ,"Gluten"
  ,"Diphosphate"
  ,"Amidon de pommes de terre"
  ,"Huile d'olive"
  ,"Lait entier en poudre"
  ,"Pomme de terre"
  ,"Pomme"
  ,"Concentrato di pomodoro"
  ,"Arôme naturel de vanille"
  ,"Moutarde"
  ,"Sirop de glucose-fructose"
  ,"Persil"
  ,"Petit-lait"
  ,"Matière grasse lactique"
  ,"Amidon transformé de maïs"
  ,"Fruit rouge"
  ,"Noisette"
  ,"nitrite-de-sodium"
  ,"Vinaigre d'alcool"
  ,"Amande"
  ,"Céleri"
  ,"Malt"
  ,"Semoule"
  ,"Disaccharide"
  ,"Semoule de blé"
  ,"Blé dur"
  ,"Gomme guar"
  ,"Enzyme"
  ,"E160"
  ,"Riz"
  ,"Palme"
  ,"france	"
  ,"Semoule de blé dur"
  ,"Ascorbate de sodium"
  ,"Huile d'olive vierge"
  ,"Jus concentré de citron"
  ,"Tournesol"
  ,"Caramel"
  ,"Gluten de blé"
  ,"Blanc d'œuf"
  ,"Amidon de froment"
  ,"Carbonates de sodium"
  ,"Purée"
  ,"Huile d'olive extra vierge"
  ,"Jaune d'œuf"
  ,"Matière grasse végétale"
  ,"Malt d'orge"
  ,"Fruits des bois"
  ,"Raisin"
  ,"Orange"
  ,"Lactosérum en poudre"
  ,"Sorbate de potassium"
  ,"Noix de coco"
  ,"Jus de citron"
  ,"Enzyme coagulante"
  ,"Graine de moutarde"
  ,"Lécithine de tournesol"
  ,"piment	"
  ,"Miel"
  ,"farine-de-riz"
  ,"Basilic"
  ,"Minéral"
  ,"Édulcorant"
  ,"Ingrédient"
  ,"Extrait de paprika"
  ,"Curcuma"
  ,"Carbonate acide de sodium"
  ,"Sésame"
  ,"Fruits de mer"
  ,"Cacao maigre en poudre"
  ,"Maïs"
  ,"Présure"
  ,"Vin"
  ,"Extrait de levure"
  ,"Œuf entier"
  ,"Agent levant"
  ,"E 500"
  ,"Beurre concentré"
  ,"Amidon de tapioca"
  ,"Sous-produit du porc"
  ,"Protéines végétales"
  ,"Sucre inverti"
  ,"Avoine"
  ,"Carraghénane"
  ,"Lait entier"
  ,"Huile de coco"
  ,"Sirop de sucre"
  ,"Caroube"
  ,"Sorbitol"
  ,"Agent de traitement de la farine"
  ,"Œuf frais"
  ,"Thym"
  ,"Carmin"
  ,"Poireau"
  ,"e250	"
  ,"Fraise"
  ,"Romarin"
  ,"Exhausteur de goût"
  ,"Framboise"
  ,"Coriandre"
  ,"Crustacés"
  ,"Sirop de sucre inverti"
  ,"Farine de graines de caroube"
  ,"Jambon de porc"
  ,"Mollusque"
  ,"Poivron rouge"
  ,"Vin blanc"
  ,"Paprika"
  ,"Cacahuète"
  ,"Sauce"
  ,"Acide lactique"
  ,"Lait écrémé"
  ,"Chapelure"
  ,"Triphosphate"
  ,"Vanille"
  ,"sodium-citrate	"
  ,"Extrait de malte d'orge"
  ,"Noix de muscade"
  ,"Cannelle"
  ,"Calcium"
  ,"Protéines de pois"
  ,"Gingembre"
  ,"E202"
  ,"Amidon modifié de pomme de terre"
  ,"Chocolat au lait"
  ,"Vitamine D"
  ,"Échalote"
  ,"Chocolat"
  ,"Champignon"
  ,"Origan"
  ,"Courgette"
  ,"Pâte"
  ,"Sulfite"
  ,"Lait de vache"
  ,"Viande de bœuf"
  ,"Poivre noir"
  ,"Emmental"
  ,"Diphosphate disodique"
  ,"Chocolat noir"
  ,"Niacine"
  ,"Herbe et épice"
  ,"Glycérol"
  ,"Saumon"
  ,"carotenoide"
  ,"Graisse végétale de palme"
  ,"Olives"
  ,"Abricot"
  ,"Acétates de sodium"
  ,"Fer"
  ,"Ananas"
  ,"Vinaigre de vin"
  ,"Phosphates de calcium"
  ,"Flocons d'avoine complète"
  ,"Extrait de vanille"
  ,"Viande de poulet"
  ,"Vitamine A"
  ,"Acide folique"
  ,"Humectant"
  ,"Amidon de riz"
  ,"Moutarde de dijon"
  ,"Poivre blanc"
  ,"Graine de sésame"
  ,"Graisse"
  ,"Poudre d'ail"
  ,"Carraghenane"
  ,"Garniture"
  ,"dont-lait"
  ,"Sel de Guérande"
  ,"Cacao en poudre"
  ,"Oignon en poudre"
  ,"Farine complète"
  ,"Graines de cumin"
  ,"Protéine végétale hydrolysée"
  ,"Lait de vache pasteurisé"
  ,"Polyphosphate"
  ,"cacao-en-poudre"
  ,"Fève de soja"
  ,"Pectine de fruit"
  ,"Huile de palmiste"
  ,"Thiamine"
  ,"Mangue"
  ,"Anthocyane"
  ,"Esters d'acides gras alimentaires"
  ,"Ciboulette"
  ,"Banane"
  ,"Blanc d'œuf en poudre"
  ,"Raisin sec"
  ,"Orge"
  ,"Café"
  ,"Farine de mais"
  ,"Épinard"
  ,"Jus d'orange"
  ,"Jambon"
  ,"Chocolat en poudre"
  ,"Porc"
  ,"Poulet"
  ,"Œuf entier frais"
  ,"Jus de légumes"
  ,"Maltitol"
  ,"Sirop de glucose de blé"
  ,"lait-demi-ecreme"
  ,"sodium-bicarbonate	"
  ,"Olive verte"
  ,"Lactate de potassium"
  ,"Glycérol"
  ,"Jus de carotte"
  ,"Amande en poudre"
  ,"Crème en poudre"
  ,"karite	"
  ,"Œuf en poudre"
  ,"Cabillaud"
  ,"Sardine"
  ,"Jus de citron vert"
  ,"Pain"]
/*initiate the autocomplete function on the "myInput" element, and pass along the countries array as possible autocomplete values:*/
autocomplete(document.getElementById("imp"), produits);
